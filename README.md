Nextag
======

Move to next and previous tags in a SGML document, including XML and HTML, in
Vim. Use `<leader>.` and `<leader>,` to search for the next and previous tags,
respectively. Prepended counts work, e.g. `5<leader>.` as do operations, e.g.
`d<leader>.` and visual mode e.g. `v<leader>,`. Doesn't match SGML comments or
`!DOCTYPES`.

*   `[operator][count]<leader>.` -- Move to start of next tag, if one exists.
*   `[operator][count]<leader>,` -- Move to start of previous tag, if one
    exists.

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
