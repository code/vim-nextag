"
" nextag.vim - Move to next and previous tags in a SGML document, including
" XML and HTML. Use <leader>. and <leader>, to search for the next and
" previous tags, respectively. Prepended counts work, e.g. 5<leader>. as do
" operations, e.g. d<leader>. and visual mode e.g. v<leader>,. Doesn't match
" SGML comments or !DOCTYPES.
"
" Maintainer: Tom Ryder <http://www.sanctum.geek.nz/>
"

"
" Wrapper to prevent overloading and signal our presence, and check we're not
" in compatible mode or running a version of Vim that's too old.
"
if exists('g:loaded_nextag') || &compatible || v:version < 700
  finish
endif
let g:loaded_nextag = 1

let s:tag = '\m<\/\?\w\+[^>]*>'

"
" Search for a SGML tag with the specified flag.
"
function! s:MoveToSGMLTag(direction, mode)
  if a:mode ==# 'v'
    normal! gv
  endif
  let l:visualfix = a:mode ==# 'v'
        \ && a:direction ==# 'next'
        \ && &selection !=# 'exclusive'
        \ ? 1
        \ : 0
  for l:iteration in range(1, v:count1)
    call search(s:tag, a:direction ==# 'next' ? 'W' : 'Wb')
    if l:visualfix
      normal! l
    endif
  endfor
  if l:visualfix
    normal! h
  endif
endfunction

"
" Default remappings.
"
nnoremap <silent>
      \ <leader>.
      \ :<C-U>call <SID>MoveToSGMLTag('next', 'n')<CR>
nnoremap <silent>
      \ <leader>,
      \ :<C-U>call <SID>MoveToSGMLTag('prev', 'n')<CR>
onoremap <silent>
      \ <leader>.
      \ :<C-U>call <SID>MoveToSGMLTag('next', 'o')<CR>
onoremap <silent>
      \ <leader>,
      \ :<C-U>call <SID>MoveToSGMLTag('prev', 'o')<CR>
vnoremap <silent>
      \ <leader>.
      \ :<C-U>call <SID>MoveToSGMLTag('next', 'v')<CR>
vnoremap <silent>
      \ <leader>,
      \ :<C-U>call <SID>MoveToSGMLTag('prev', 'v')<CR>
